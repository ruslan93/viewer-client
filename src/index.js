import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import reportWebVitals from './reportWebVitals';
import Carousel from "./components/Carusel";

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <div id="container">
    <header>
      Code Development Project
    </header>
    <div id="main" role="main">
      <Carousel />
    </div>
  </div>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
