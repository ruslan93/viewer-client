import React, {useEffect, useState} from 'react';
import AliceCarousel from 'react-alice-carousel';
import 'react-alice-carousel/lib/alice-carousel.css';
import '../index.css';
import {LazyLoader} from "./LazyLoader";

const thumbItems = (items, [setThumbIndex, setThumbAnimation, mainIndex]) => {
  return items.map((item, i) => (
    <div className={`thumbnails-wrap ${mainIndex === i ? 'active' : ''}`} title={item.id} key={item.id + i}>
      <LazyLoader className="thumbnails-img" src={`../images/thumbnails/${item.thumbnail}`} alt={item.thumbnail} onClick={() => (setThumbIndex(i), setThumbAnimation(true))}/>
      <span>{item.id}</span>
    </div>
  ));
};

const largeItems = (items) => {
  return items.map((item, i) => (
    <div className="group" key={item.thumbnail + i}>
      <LazyLoader src={`../images/large/${item.image}`} alt={item.image} width="430" height="360"/>
      <div className="details">
        <p><strong>Title</strong> {item.title}</p>
        <p><strong>Description</strong> {item.description}</p>
        <p><strong>Cost</strong> ${item.cost}</p>
        <p><strong>ID #</strong> {item.id}</p>
        <p><strong>Thumbnail File</strong> {item.thumbnail}</p>
        <p><strong>Large Image File</strong> {item.image}</p>
      </div>
    </div>
  ));
};

const Carousel = () => {
  const [currentPage, setCurrentPage] = useState(0);
  const [page, setPages] = useState(0);
  const [mainIndex, setMainIndex] = useState(0);
  const [totalItems, setTotalItems] = useState(0);
  const [mainAnimation, setMainAnimation] = useState(false);
  const [thumbIndex, setThumbIndex] = useState(0);
  const [thumbAnimation, setThumbAnimation] = useState(false);
  const [url, setUrl] = useState(
    `${process.env.REACT_APP_API_URL}/medias?page=${currentPage}&size=4`,
  );
  const [thumbs, setThumbs] = useState([]);
  const [large, setLarge] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const response = await fetch(
        url,
        {
          mode: 'cors',
          headers: {
            'Access-Control-Allow-Origin':'*'
          }
        }
      );

      const { medias, currentPage, totalItems, pages } = await response.json()

      setThumbs(thumbItems(medias, [setThumbIndex, setThumbAnimation, mainIndex]));
      setLarge(largeItems(medias));
      setCurrentPage(currentPage);
      setPages(pages);
      setTotalItems(totalItems);
    };

    fetchData();

  }, [url]);

  const slideNext = () => {
    if(currentPage < page - 1) {
      const page = currentPage + 1
      setCurrentPage(page)
      setThumbAnimation(true);
      setUrl(`${process.env.REACT_APP_API_URL}/medias?page=${page}&size=4`)
    }

  };

  const slidePrev = () => {
    if(currentPage !== 0) {
      const page = currentPage - 1
      setCurrentPage(page)
      setThumbAnimation(true);
      setUrl(`${process.env.REACT_APP_API_URL}/medias?page=${page}&size=4`)
    }

  };

  const syncMainBeforeChange = (e) => {
    setMainAnimation(true);
  };

  const syncMainAfterChange = (e) => {
    setMainAnimation(false);

    if (e.type === 'action') {
      setThumbIndex(e.item);
      setThumbAnimation(false);
    } else {
      setMainIndex(thumbIndex);
    }
  };

  const syncThumbs = (e) => {
    setThumbIndex(e.item);
    setThumbAnimation(false);

    if (!mainAnimation) {
      setMainIndex(e.item);
    }
  };

  return [
    <div id="large">
      <AliceCarousel
        activeIndex={mainIndex}
        animationType="fadeout"
        animationDuration={400}
        disableDotsControls
        disableButtonsControls
        mouseTracking={!thumbAnimation}
        items={large}
        onSlideChange={syncMainBeforeChange}
        onSlideChanged={syncMainAfterChange}
        touchTracking={!thumbAnimation}
      />
    </div>,
    <div className="thumbnails">
      <div className="group">
        <AliceCarousel
          activeIndex={thumbIndex}
          autoWidth
          disableDotsControls
          items={thumbs}
          disableButtonsControls
          mouseTracking={false}
          onSlideChanged={syncThumbs}
          touchTracking={!mainAnimation}
        />
        <span className={`previous ${currentPage === 0 ? 'disabled' : ''}`} onClick={slidePrev} title="Previous">Previous</span>
        <span className={`next ${currentPage < page - 1 ? '' : 'disabled'}`} title="Next" onClick={slideNext}>Next</span>
      </div>
    </div>
  ]
};

export default Carousel
