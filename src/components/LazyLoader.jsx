import { useState, useEffect } from 'react';

export function LazyLoader(props) {
  let timerId;
  const { src = '', delay = 0, onLoad, alt, className } = props;
  const [isMounted, setMounted] = useState(false);
  const [isLoading, setLoading] = useState(true);

  function loadImage() {
    const image = new Image();

    image.src = src;
    image.alt = alt;
    image.onload = () => {
      setLoading(false);
      onLoad();
    };
    image.onerror = () => {
      setLoading(false);
    };
  }

  useEffect(() => {
    if (!isMounted) {
      setMounted(true);
      delay ? (timerId = setTimeout(loadImage, delay)) : loadImage();
    }
    return () => clearTimeout(timerId);
  }, [isLoading]);

  return isLoading ? <div className={className}>Loading...</div> : <img {...props} src={src} />;
}
